try:
	import Tkinter as tk
	from tkColorChooser import askcolor
except ImportError:
	import tkinter as tk
	from tkinter.colorchooser import askcolor

def getColor():
	return askcolor()

