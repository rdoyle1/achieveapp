import sys, os, json
try:
	import urllib2 as get
except ImportError:
	import urllib.request as get

def get_input(val):
	try:
		inpt = raw_input(val)
	except NameError:
		inpt = input(val)
	return inpt

def getTitle(buf):
	start = buf.find(b"Steam Community :: ")
	end = buf.find(b" :: @")
	endUser = buf.find(b"</title>")
	return buf[start+19:end],buf[end+5:endUser]

def isUnlocked(buf):
	achvTxtLoc = buf.find(b"\"achieveTxt\"")
	achvUnlLoc = buf.find(b"\"achieveUnlockTime\"")
	achvRowLoc = buf.find(b"\"achieveRow\"")
	if achvTxtLoc < achvUnlLoc < achvRowLoc:
		return True
	return False

def getAchName(buf):
	titleLoc = buf.find(b"<h3 class=\"ellipsis\">")
	titleEnd = buf.find(b"</h3>")
	return buf[titleLoc+21:titleEnd]

def getAchDesc(buf,unlocked):
	descLoc = 0
	descLen = 0
	if unlocked:
		descLoc = buf.find(b"<h5>")
		descLen = 4
	else:
		descLoc = buf.find(b"<h5 class=\"ellipsis\">")
		descLen = 21
	descEnd = buf.find(b"</h5>")
	return buf[descLoc+descLen:descEnd]

def main1():
	return {"test":["testing","https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png"]},{}

def main(url):
	# We have a dictionary of both locked and unlocked achievements.
	unlocked = {}
	locked = {}

	title,user = ("Empty","Nobody")
	# The given URL is your achievement page for Remember Me. First, we get the page
	try:
		response = get.urlopen(str(url)).read()
	except get.HTTPError, e:
		print(e.code)
		return title,user,unlocked,locked
	except get.URLError, e:
		print(e.args)
		return title,user,unlocked,locked

	title,user = getTitle(response)
	# We first find the beginning and end of the achievement list on the page to cut down on the parsing
	start_index = response.find(b"achievements_list")
	end_index = response.find(b"responsive_page_legacy_content")
	response = response[start_index:end_index]

	# This is the index of the 'start' of the achievement list.
	index = response.find(b"achieveImgHolder")

	# We'll loop until there are no achievements left
	while index>=0:
		index+=16 # Move past the ImgHolder tag so we don't infinite loop.
		response = response[index:]

		imgloc = response.find(b"src=")
		imgurl = response[imgloc+5:imgloc+124] # Get the image url
		if isUnlocked(response): # is the achievement unlocked? 
			unlocked[getAchName(response)] = [getAchDesc(response,True),imgurl] # add entry to unlocked dict
		else:
			locked[getAchName(response)] = [getAchDesc(response,False),imgurl] # add to locked dict
		index = response.find(b"achieveImgHolder") # continue loop
	
	if __name__=="__main__":
		# Print out the achievements
		printDict(locked,"Locked")
		printDict(unlocked,"Unlocked")
	
	# Check if we have a cache of previous achievements and compare to see if there are changes
	newUnlock, newLock = checkCache(unlocked,locked)
	if len(newUnlock)>0 or len(newLock)>0:
		saveCache(unlocked,locked)
	return title,user,unlocked,locked

def printDict(dictionary, header):
	print(header)
	for row in dictionary:
		desc = dictionary[row][0]
		address = dictionary[row][1]
		print("{}: {}\n     {}\n".format(row,desc,address))


"""
Check if we have a cache of achievements, and if we do, check the cache entries
against our unlocks to see if there are changes
"""
def checkCache(unlocked,locked):
	data = []
	retUnlock = {}
	retLock = {}
	if not os.path.isfile("saveGameStats.log") or os.stat("saveGameStats.log").st_size == 0:
		return unlocked,locked
	else:
		with open("saveGameStats.log") as f:
			for line in f:
				data.append(json.loads(line))
		cacheUnlock = data[0]
		cacheLock = data[1]
		for key in unlocked:
			key=str(key)
			if key not in cacheUnlock:
				retUnlock[key] = unlocked[key]
		for key in locked:
			key=str(key)
			if key not in cacheLock:
				retLock[key] = locked[key]
		return retUnlock, retLock

def saveCache(unlocked,locked):
	with open("saveGameStats.log", 'w') as f:
		json.dump(str(unlocked),f)
		f.write("\n")
		json.dump(str(locked),f)

if __name__=="__main__":
	main("http://steamcommunity.com/profiles/76561198026045535/stats/RememberMe")
