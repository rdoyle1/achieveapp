#!/usr/bin/env python
from threading import Timer
import sys, math, re, time
from sys import platform as _platform
from steam import Steam
import colors
import parse_html as p
import io
# allows for image formats other than gif
from PIL import Image, ImageTk
import copy, difflib
try:
	# Python2
	import Tkinter as tk
	import tkMessageBox as messagebox
	from urllib2 import urlopen
except ImportError:
	# Python3
	import tkinter as tk
	from tkinter import messagebox
	from urllib.request import urlopen

# Need to make sure that code is portable between the important systems
def checkOS():
	if _platform == "linux" or _platform == "linux2":
		return 0
	elif _platform == "darwin":
		return 1
	elif _platform == "win32":
		return 2

def setTransparency(window,value):
	_os = checkOS()
	if _os:
		window.attributes('-alpha',value)
	else:
		window.wait_visibility(window)
		window.wm_attributes('-alpha',value)

class popupSelect(object):
	def __init__(self,master,choice,options):
		self.master=master
		self.choice=choice
		self.options=options
		self.value = None
		self.options.append("<None>")
		top=self.top=tk.Toplevel(master,padx=10,pady=10)
		top.title("Alternate Choices")
		self.label=tk.Label(top,text="{} isn't ont of the user's games. Did you mean one of the following?".format(choice))
		self.label.grid(row=0)

		self.v = tk.StringVar()
		self.v.set("default")

		for i,alternate in enumerate(self.options):
			b = tk.Radiobutton(top, text = alternate, variable = self.v, value = alternate)
			b.grid(row=i+1,sticky=tk.W)

		self.done=tk.Button(top,text="Submit",command = self.submit,default=tk.ACTIVE,highlightthickness=0)
		self.done.grid(row=len(self.options)+1,column=0)

		self.cancel=tk.Button(top,text="Cancel",command = self.cleanup,highlightthickness=0)
		self.cancel.grid(row=len(self.options)+2,column=0)

		self.top.bind('<Escape>',self.cleanup)
		self.top.bind('<Return>',self.submit)

	def submit(self,event=None):
		self.value = self.v.get()
		if self.value=="<None>":
			self.value=None
		self.cleanup()
	
	def cleanup(self,event=None):
		self.top.destroy()

class FloatingWindow(tk.Toplevel):
	def __init__(self, *args, **kwargs):
		if 'slave' in kwargs:
			self.slave = kwargs.pop('slave')
		else:
			self.slave = True
		color = False
		for key in ['bg','background']:
			if key in kwargs:
				self.color=kwargs.pop(key)
				color = True
		if not color:
			self.color=((0,0,0), '#000000')
		color = False
		for key in ['fg','foreground']:
			if key in kwargs:
				self.text=kwargs.pop(key)
				color = True
		if not color:
			self.text=((255,255,255), '#ffffff')
		tk.Toplevel.__init__(self, *args, **kwargs)
		self.overrideredirect(True)
		self.is_slave = False
		self.windows = {}
		self.boundaries = [[0,self.winfo_screenwidth()],[0,self.winfo_screenheight()]]

		self.grip = tk.Label(self,bg=self.color[1],fg=self.text[1])
		#self.grip.grid(row=0,rowspan=3,columnspan=3,sticky=tk.W+tk.E+tk.S+tk.N)

		self.grip.bind("<ButtonPress-1>", self.StartMove)
		self.grip.bind("<ButtonRelease-1>", self.StopMove)
		self.grip.bind("<B1-Motion>", self.OnMotion)
		self.update=0
	
	def setSlave(self,master):
		self.is_slave=True
		self.master=master
		self.grip.unbind("<ButtonPress-1>")
		self.grip.unbind("<ButtonRelease-1>")
		self.grip.unbind("<B1-Motion>")
		self.setChildren()
	
	def setChildren(self):
		master = None
		if self.is_slave:
			master = self.master
		else:
			master = self
		for child in self.winfo_children():
			if isinstance(child, FloatingWindow) and child._name not in master.windows:
				child.setSlave(master)
				master.windows[child._name]=child
				master.grip.bind("<ButtonPress-1>", child.StartMove,'+')
				master.grip.bind("<ButtonRelease-1>", child.StopMove,'+')
				master.grip.bind("<B1-Motion>", child.OnMotion,'+')
	
	def moveChild(self,event):
		self.setChildren()
		for key in self.windows:
			self.windows[key].StartMove(event)

	class MoveEvent():
		def __init__(self,event):
			self.x=event.x
			self.y=event.y
			self.char=event.char
			self.delta=event.delta
			self.height=event.height
			self.width=event.width
			self.keycode=event.keycode
			self.keysym=event.keysym
			self.keysym_num=event.keysym_num
			self.num=event.num
			self.send_event=event.send_event
			self.serial=event.serial
			self.state=event.state
			self.time=event.time
			self.type=event.type
			self.widget=event.widget
			self.x_root=event.x_root
			self.y_root=event.y_root

		def __repr__(self):
			return "MoveEvent()"

		def __str__(self):
			return "x: {}, y: {}\nchar: {}, delta: {}\nheight: {}, keycode: {}\nkeysym: {}, keysym_num: {}\nnum: {}, send_event: {}\nserial: {}, state: {}\ntime: {}, type: {}\nwidget: {}, width: {}\nx_root: {}, y_root: {}".format(self.x, self.y, self.char, self.delta, self.height, self.keycode, self.keysym, self.keysym_num, self.num, self.send_event, self.serial, self.state, self.time, self.type, self.widget, self.width, self.x_root, self.y_root)
	
	def alpha(self,value):
		setTransparency(self,value)
	
	def printEvent(self,event):
		print "x: {}, y: {}\nchar: {}, delta: {}\nheight: {}, keycode: {}\nkeysym: {}, keysym_num: {}\nnum: {}, send_event: {}\nserial: {}, state: {}\ntime: {}, type: {}\nwidget: {}, width: {}\nx_root: {}, y_root: {}".format(event.x, event.y, event.char, event.delta, event.height, event.keycode, event.keysym, event.keysym_num, event.num, event.send_event, event.serial, event.state, event.time, event.type, event.widget, event.width, event.x_root, event.y_root)

	def SetGrip(self):
		cols,rows = self.grid_size()
		if cols==0:
			cols=1
		if rows==0:
			rows=1
		self.grip.grid(row=0,column=0,rowspan=rows,columnspan=cols,sticky=tk.W+tk.E+tk.S+tk.N)

	def StartMove(self, event):
		self.x = event.x
		self.y = event.y
		self.update=event.serial
		if self.slave and not self.is_slave:
			child = self.MoveEvent(event)
			self.moveChild(child)

	def StopMove(self, event):
		self.x = None
		self.y = None
		self.windows={}

	def OnMotion(self, event):
		if event.serial<self.update:
			return
		else:
			self.update=event.serial
		deltax = event.x - self.x
		deltay = event.y - self.y
		x = self.winfo_x() + deltax
		y = self.winfo_y() + deltay
		if x<self.boundaries[0][0]:
			x=0
			event.x=self.x
		elif x>self.boundaries[0][1]-self.winfo_width():
			x=self.boundaries[0][1]-self.winfo_width()
			event.x=self.x
		if y<self.boundaries[1][0]:
			y=0
			event.y=self.y
		elif y>self.boundaries[1][1]-self.winfo_height():
			y=self.boundaries[1][1]-self.winfo_height()
			event.y=self.y
		self.geometry("+%d+%d" % (x, y))

class popupMenu(object):
	def __init__(self,master,interval,transition,key,user,game,color,text):
		self.interval=interval
		self.transition=transition
		self.key = key
		self.user = user
		self.game = game
		self.master = master
		self.color=color
		self.text=text
		top=self.top=FloatingWindow(master,bg=color,fg=text)
		#top=self.top=tk.Toplevel(master)
		top.title("Options Menu")
		self.URLvalue=None
		self.DRAWvalue=False
		test=self.test=FloatingWindow(top)
		#test.SetGrip()

		self.steamKEY=tk.Button(top,text="Enter Steam Key",command = self.getKEY,highlightthickness=0,highlightbackground=self.color[1])
		self.steamKEY.grid(row=1,column=0,padx=10,pady=10)

		self.steamUSER=tk.Button(top,text="Enter Steam Username",command = self.getUSER,highlightthickness=0,highlightbackground=self.color[1])
		self.steamUSER.grid(row=1,column=1,padx=10,pady=10)

		self.steamGAME=tk.Button(top,text="Enter Steam Game",command = self.getGAME,highlightthickness=0,highlightbackground=self.color[1])
		self.steamGAME.grid(row=1,column=2,padx=10,pady=10)

		self.draw=tk.Button(top,text="Toggle Draw Style",command = self.changeDraw,highlightthickness=0,highlightbackground=self.color[1])
		self.draw.grid(row=1,column=3,padx=10,pady=10)

		self.cancel=tk.Button(top,text='Done',command = self.cleanup,highlightthickness=0,highlightbackground=self.color[1])
		self.cancel.grid(row=2,column=3,padx=10,pady=10)

		self.changeInt=tk.Button(top,text='Change Fade Interval',command = self.changeFade,highlightthickness=0,highlightbackground=self.color[1])
		self.changeInt.grid(row=2,column=0,padx=10,pady=10)

		self.bgButton=tk.Button(top,text='Set Background Color',command=self.setColor,highlightthickness=0,highlightbackground=self.color[1])
		self.bgButton.grid(row=2,column=1,padx=10,pady=10)

		self.fgButton=tk.Button(top,text='Set Text Color',command=self.setText,highlightthickness=0,highlightbackground=self.color[1])
		self.fgButton.grid(row=2,column=2,padx=10,pady=10)

		top.SetGrip()
		#self.top.bind('<Escape>',self.cleanup)
		#self.top.bind('<Return>',self.cleanup)

	def setColor(self):
		self.disableButtons()
		color = colors.getColor()
		self.enableButtons()
		self.top.lift()
		if color!=(None,None):
			self.color=color

	def setText(self):
		self.disableButtons()
		color = colors.getColor()
		self.enableButtons()
		self.top.lift()
		if color!=(None,None):
			self.text=color

	def disableButtons(self):
		self.draw.config(state=tk.DISABLED)
		self.cancel.config(state=tk.DISABLED)
		self.bgButton.config(state=tk.DISABLED)
		self.fgButton.config(state=tk.DISABLED)
		self.changeInt.config(state=tk.DISABLED)
		self.steamKEY.config(state=tk.DISABLED)
		self.steamUSER.config(state=tk.DISABLED)
		self.steamGAME.config(state=tk.DISABLED)

	def enableButtons(self):
		self.draw.config(state=tk.NORMAL)
		self.cancel.config(state=tk.NORMAL)
		self.bgButton.config(state=tk.NORMAL)
		self.fgButton.config(state=tk.NORMAL)
		self.changeInt.config(state=tk.NORMAL)
		self.steamKEY.config(state=tk.NORMAL)
		self.steamUSER.config(state=tk.NORMAL)
		self.steamGAME.config(state=tk.NORMAL)

	def changeFade(self):
		self.w = popupScale(self.master,self.interval,self.transition)
		self.disableButtons()
		self.master.wait_window(self.w.top)
		self.enableButtons()
		self.top.lift()
		self.interval = self.w.interval
		self.transition = self.w.transition
		self.cleanup()

	def cleanup(self,event=None):
		self.top.destroy()

	def __isInt(self,val):
		try:
			int(val)
		except ValueError:
			return False
		return True

	def getKEY(self):
		self.w = popupEntry(self.master,"API Key", "Enter Steam API Key",self.key)
		self.disableButtons()
		self.master.wait_window(self.w.top)
		self.enableButtons()
		self.top.lift()
		val = self.w.value
		if val!=None and val!="":
			self.key = val.strip()
	
	def getUSER(self):
		self.w = popupEntry(self.master,"Steam Username", "Enter Steam Username/ID",self.user)
		self.disableButtons()
		self.master.wait_window(self.w.top)
		self.enableButtons()
		self.top.lift()
		val = self.w.value
		if val!=None and val!="":
			if self.__isInt(val):
				self.user = int(val.strip())
			else:
				self.user = val.strip()
	
	def getGAME(self):
		self.w = popupEntry(self.master,"Steam Game", "Enter Game Name/ID",self.game)
		self.disableButtons()
		self.master.wait_window(self.w.top)
		self.enableButtons()
		self.top.lift()
		val = self.w.value                                                              
		if val!=None and val!="": 
			if self.__isInt(val):
				self.game = int(val.strip())
			else:
				self.game = val.strip()

	def changeDraw(self):
		self.DRAWvalue=True
		self.cleanup()

class popupScale(object):
	def __init__(self,master,interval,transition):
		top=self.top=tk.Toplevel(master,padx=10,pady=10)
		top.title("Fade Interval")
		self.intLab=tk.Label(top,text="Interval between transitions (seconds)")
		self.intLab.grid(row=0,column=1)

		self.interval=interval
		self.transition=transition
		self.intervar = tk.DoubleVar()
		self.intervar.set(interval)
		self.transvar = tk.DoubleVar()
		self.transvar.set(transition)

		self.intScale=tk.Scale(top,variable=self.intervar,from_=0.1,to=60.,resolution=1.,orient=tk.HORIZONTAL,width=20,length=200)
		self.intScale.set(self.interval)
		self.intScale.grid(row=1,column=0, columnspan=2)

		self.tranLab=tk.Label(top,text="Transition length (seconds)")
		self.tranLab.grid(row=2,column=1)

		self.tranScale=tk.Scale(top,variable=self.transvar,from_=0.1,to=10.,resolution=0.1,orient=tk.HORIZONTAL,width=20,length=200)
		self.tranScale.set(self.transition)
		self.tranScale.grid(row=3,column=0, columnspan=2)

		self.done=tk.Button(top,text='Enter',command=self.cleanup,default=tk.ACTIVE,highlightthickness=0)
                self.done.grid(row=1,column=2)
		self.top.bind('<Return>',self.cleanup)

        def cleanup(self,event=None):
		self.interval=self.intervar.get()
                self.transition=self.transvar.get()
                self.top.destroy()

class popupEntry(object):
	def __init__(self,master,title,label,default):
		top=self.top=tk.Toplevel(master,padx=10,pady=10)
		top.title(title)
		self.default = default
		self.l=tk.Label(top,text=label)
		self.value=None
		self.v = tk.StringVar()
		self.v.set(default)
		self.l.grid(row=0,column=1,padx=5,pady=5)
		self.e=tk.Entry(top,textvariable=self.v)
		self.e.grid(row=1,column=1,padx=5,pady=5)
		self.b=tk.Button(top,text='Enter',command=self.cleanup,default=tk.ACTIVE,highlightthickness=0)
		self.top.bind('<Return>',self.cleanup)
		self.b.grid(row=1,column=2,padx=5,pady=5)
		self.exit=tk.Button(top,text='Cancel',command = self.cancel,highlightthickness=0)
		self.exit.grid(row=2,column=1,padx=5,pady=5)

	def cancel(self,event=None):
		self.value = self.default
		self.top.destroy()
	
	def cleanup(self,event=None):
		self.value=self.e.get()
		self.top.destroy()
	
	def exit(self):
		self.value=None
		self.top.destroy()

class App(tk.Frame):
	def __init__(self,master):
		tk.Frame.__init__(self,master)
		self.master=master
		self.color=((0, 0, 0), '#000000')
		self.text=((255,255,255), '#ffffff')
		self.key=""
		self.user=""
		self.game=""
		self.api = None
		self.do_load=True
		self._timer = None
		self.interval=5.
		self.transition=1.
		self.transparency=1.
		self.configure(background=self.color[1])
		self.rowconfigure(0)
		self.rowconfigure(1,weight=1)
		self.rowconfigure(2)
		self.columnconfigure(0,weight=1)
		self.columnconfigure(1,weight=1)
		self.locTitleVar = tk.StringVar()
		self.unlocTitleVar = tk.StringVar()
		self.genInfoVar = tk.StringVar()
		self.locTitle = tk.Label(self, bg=self.color[1],fg=self.text[1], textvariable=self.locTitleVar, font=("Helvetica",-24,"bold italic"), padx=5,pady=5)
		self.unlocTitle = tk.Label(self, bg=self.color[1],fg=self.text[1], textvariable=self.unlocTitleVar, font=("Helvetica",-24,"bold italic"), padx=5,pady=5)
		self.GeneralInfo = tk.Label(self,bg=self.color[1],fg=self.text[1], textvariable=self.genInfoVar, font=("Helvetica",-18), padx=5,pady=5)
		self.locTitleVar.set("Incomplete Achievements")
		self.unlocTitleVar.set("Complete Achievements")
		self.locTitle.grid(row=0,column=0)
		self.unlocTitle.grid(row=0,column=1)
		self.GeneralInfo.grid(row=3,columnspan=2)
		
		self.urlButton=tk.Button(self,text="Options",command=self.popup,highlightthickness=0,highlightbackground=self.color[1])
		self.urlButton.grid(row=2,column=1)
		
		self.updateButton = tk.Button(self,text="Refresh Achievements",command=self.update,default=tk.ACTIVE,highlightthickness=0,highlightbackground=self.color[1])
		self.updateButton.grid(row=2,column=0)
		self.bind("<Return>", self.update)
		
		self.locked = None
		self.unlocked = None
		self.lockedFrame = None
		self.unlockedFrame = None
		self.regex = re.compile('[^a-zA-Z]')
		self.updateMethod = 1
		self.update()
		self.bind("<Configure>", self.resize)

	def disableButtons(self):
		self.updateButton.config(state=tk.DISABLED)
		self.urlButton.config(state=tk.DISABLED)

	def enableButtons(self):
		self.updateButton.config(state=tk.NORMAL)
		self.urlButton.config(state=tk.NORMAL)

	def setInfo(self,text):
		self.genInfoVar.set(text)

	def setColors(self):
		self.config(bg=self.color[1])
		self.locTitle.config(bg=self.color[1],fg=self.text[1])
		self.unlocTitle.config(bg=self.color[1],fg=self.text[1])
		self.GeneralInfo.config(bg=self.color[1],fg=self.text[1])
		self.lockedFrame.config(bg=self.color[1])
		self.unlockedFrame.config(bg=self.color[1])
		self.urlButton.config(highlightbackground=self.color[1])
		self.updateButton.config(highlightbackground=self.color[1])
		
		self.alpha(self.transparency)
	
	def alpha(self,value):
		setTransparency(self.master,value)

	def popup(self):
		self.w = popupMenu(self.master,self.interval,self.transition,self.key,self.user,self.game,self.color,self.text)
		self.disableButtons()
		self.master.wait_window(self.w.top)
		self.enableButtons()
		self.interval = self.w.interval
		self.transition = self.w.transition
		self.color=self.w.color
		self.text=self.w.text
		self.setColors()
		if self.w.DRAWvalue:
			self.changeDraw()
		if self.w.key!=self.key and self.w.key!="":
			self.key = self.w.key
			self.api = Steam(self.key)
		if self.w.user!=self.user and self.w.user!="":
			self.user = self.w.user
			if self.api!=None:
				ret = self.api.user(self.user)
				if ret==1:
					messagebox.showinfo("No User Found", "Couldn't find user \"{}\".".format(self.user))
					self.user=""
				elif ret==2:
					messagebox.showinfo("API Key Error", "Try re-entering your Steam API key.")
					self.key=""
		if self.w.game!=self.game and self.w.game!="":
			self.game = self.w.game
		if self.key!="" and self.user!="" and self.game!="":
			self.do_load=True
			self.update()

	def tag(self,key):
		tag = "".join(str(key.strip()).split())
		return self.regex.sub('',tag)

	def resize(self,event=None):
		if self.updateMethod:
			self.gridResize(event)
		else:
			self.blendResize(event)
		self.locTitle.config(wraplength=self.winfo_width()/2)
		self.unlocTitle.config(wraplength=self.winfo_width()/2)
		self.GeneralInfo.config(wraplength=self.winfo_width())
	
	def blendResize(self,event=None):
		if self.do_load:
			self.do_load=False
			return
		lockedCanvas,locImg,locKeys,newLocImg,tkLocImg = self.blendData[0]
		unlockedCanvas,unlocImg,unlocKeys,newUnlocImg,tkUnlocImg = self.blendData[1]
		initial = self.blendData[2]
		tag = self.blendData[3]
		count = self.blendData[4]
		unlWid,unlHeig = (self.unlockedFrame.winfo_width(),self.unlockedFrame.winfo_height())
		locWid,locHeig = (self.lockedFrame.winfo_width(),self.lockedFrame.winfo_height())
		
		if newUnlocImg!=None:
			maxwidth,maxheight = newUnlocImg.size
			ratio = min(1.0*unlWid/maxwidth,1.0*unlHeig/maxheight)
			if ratio>1:
				ratio=1
			size = (int(maxwidth*ratio),int(maxheight*ratio))
			resized = newUnlocImg.resize(size,Image.ANTIALIAS)
			tkUnlocImg = ImageTk.PhotoImage(resized)
			if not initial:
				unlockedCanvas.delete(tag)
			unlockedCanvas.create_image((0, 0), image=tkUnlocImg, anchor=tk.NW, tags=tag)
			unlockedCanvas.config(width=int(maxwidth*ratio), height=int(maxheight*ratio))
			if initial:
				self.lockedFrame.config(width=lockedCanvas.winfo_width(), height=lockedCanvas.winfo_height())

		if newLocImg!=None:
			maxwidth,maxheight = newLocImg.size
			ratio = min(1.0*locWid/maxwidth,1.0*locHeig/maxheight)
			if ratio>1:
				ratio=1
			size = (int(maxwidth*ratio),int(maxheight*ratio))
			resized = newLocImg.resize(size,Image.ANTIALIAS)
			tkLocImg = ImageTk.PhotoImage(resized)
			if not initial:
				lockedCanvas.delete(tag)
			lockedCanvas.create_image((0, 0), image=tkLocImg, anchor=tk.NW, tags=tag)
			lockedCanvas.config(width=int(maxwidth*ratio), height=int(maxheight*ratio))
			if initial:
				self.unlockedFrame.config(width=unlockedCanvas.winfo_width(), height=unlockedCanvas.winfo_height())

		self.blendData = [(lockedCanvas,locImg,locKeys,newLocImg,tkLocImg),(unlockedCanvas,unlocImg,unlocKeys,newUnlocImg,tkUnlocImg),initial,tag,count]
	
	def gridResize(self,event):
		#print((event.width,event.height),(self.locTitle.winfo_width(),self.locTitle.winfo_height()),(self.unlocTitle.winfo_width(),self.unlocTitle.winfo_height()))
		if self.do_load:
			self.do_load=False
			return
		unlocKeys = sorted(self.unlocked.keys())
		locKeys = sorted(self.locked.keys())
		unlocLen = len(unlocKeys)
		locLen = len(locKeys)
		if unlocLen==0:
			unlocLen=1
		if locLen==0:
			locLen=1
		unlocSize = int(math.sqrt(unlocLen))
		locSize = int(math.sqrt(locLen))
		if unlocLen%unlocSize>0:
			unlocLen/=unlocSize
			unlocLen+=1
		else:
			unlocLen/=unlocSize
		if locLen%locSize>0:
			locLen/=locSize
			locLen+=1
		else:
			locLen/=locSize
		width, height = (int(1.0*event.width/(unlocSize+locSize)),int(1.0*event.height/max(unlocSize,locSize)))
		unlWid,unlHeig = (int(1.0*self.unlockedFrame.winfo_width()/unlocSize),int(1.0*self.unlockedFrame.winfo_height()/unlocLen))
		locWid,locHeig = (int(1.0*self.lockedFrame.winfo_width()/locSize),int(1.0*self.lockedFrame.winfo_height()/locLen))
		for i,key in enumerate(self.unlocked):
			tag = self.tag(key)
			#print(tag,self.unlocked[key])
			maxwidth,maxheight = self.unlocked[key][2].size
			ratio = min(1.0*unlWid/maxwidth,1.0*unlHeig/maxheight)
			#ratio = min(1.0*width/maxwidth,1.0*height/maxheight)
			if ratio>1:
				ratio=1
			size = (int(maxwidth*ratio),int(maxheight*ratio))
			#print(ratio,(self.unlockedFrame.winfo_width(),self.unlockedFrame.winfo_height()),(event.width,event.height),(width,height),(maxwidth,maxheight),size)
			resized = self.unlocked[key][2].resize(size,Image.ANTIALIAS)
			self.unlocked[key][4] = ImageTk.PhotoImage(resized)
			self.unlocked[key][3].delete(tag)
			self.unlocked[key][3].create_image((0, 0), image=self.unlocked[key][4], anchor=tk.NW, tags=tag)
			self.unlocked[key][3].config(width=int(maxwidth*ratio), height=int(maxheight*ratio))
		for i,key in enumerate(self.locked):
			tag = self.tag(key)
			#print(tag,self.locked[key])
			maxheight,maxwidth = self.locked[key][2].size
			ratio = min(1.0*locWid/maxwidth,1.0*locHeig/maxheight)
			#ratio = min(1.0*width/maxwidth,1.0*height/maxheight) 
			if ratio>1: 
				ratio=1
			size = (int(maxwidth*ratio),int(maxheight*ratio))
			#print(ratio,(self.unlockedFrame.winfo_width(),self.unlockedFrame.winfo_height()),(event.width,event.height),(width,height),(maxwidth,maxheight),size)
			resized = self.locked[key][2].resize(size,Image.ANTIALIAS)
			self.locked[key][4] = ImageTk.PhotoImage(resized)
			self.locked[key][3].delete(tag)
			self.locked[key][3].create_image((0, 0), image=self.locked[key][4], anchor=tk.NW, tags=tag)
			self.locked[key][3].config(width=int(maxwidth*ratio), height=int(maxheight*ratio))

	def giveDesc(self,key,lockOrUnlock):
		if lockOrUnlock:
			return self.locked[key][0]
		else:
			return self.unlocked[key][0]

	def initFrames(self):
		if self.lockedFrame!=None:
			self.lockedFrame.destroy()
		self.lockedFrame = tk.Frame(self,borderwidth=5,relief=tk.SUNKEN)
		self.lockedFrame.grid(row=1,column=0,sticky=tk.W+tk.E+tk.N+tk.S)
		#self.lockedFrame.pack(fill=tk.BOTH,side=tk.LEFT,expand=1)

		if self.unlockedFrame!=None:
			self.unlockedFrame.destroy()
		self.unlockedFrame = tk.Frame(self,borderwidth=5,relief=tk.SUNKEN)
		self.unlockedFrame.grid(row=1,column=1,sticky=tk.W+tk.E+tk.N+tk.S)
		#self.unlockedFrame.pack(fill=tk.BOTH,side=tk.RIGHT,expand=1)
		
		self.lockedFrame.configure(background=self.color[1])
		self.unlockedFrame.configure(background=self.color[1])

	def tryAPI(self,recurse=False):
		try:
			title, user, unlocked, locked = self.api.gameAchievements(self.game)
		except ValueError as e:
			e = e.message
			matches = difflib.get_close_matches(self.game.encode('utf-8'),e)
			if len(matches)==0 or recurse:
				messagebox.showinfo("No Game Found", "Couldn't find game \"{}\" in {}'s library.".format(self.game,self.user))
				title, user, unlocked, locked = self.game,self.user,{},{}
			else:
				self.w = popupSelect(self.master,self.game.encode('utf-8'),matches)
				self.master.wait_window(self.w.top)
				val = self.w.value
				if val!=None:
					self.game=val
					self.tryAPI(True)
					return
				title, user, unlocked, locked = self.game,self.user,{},{}
		except AttributeError as e:
			messagebox.showinfo("No Achievements", "{}".format(e))
			title, user, unlocked, locked = self.game,self.user,{},{}
		self.title, self.user, self.unlocked, self.locked = (title, user, unlocked, locked)
		#self.title, self.user, self.unlocked, self.locked = p.main(self.key)
		self.master.wm_title("{}'s {} Achievements".format(self.user,self.title))

	def update(self,event=None,initial=False):
		if self._timer!=None:
			self.master.after_cancel(self._timer)
			self._timer=None
		self.do_load=True
		self.initFrames()
		self.unlocked=self.locked=None
		if self.api==None:
			self.title=""
			self.user=""
			self.unlocked={}
			self.locked={}
			self.master.wm_title("Achievements")
		else:
			self.tryAPI()
		if self.updateMethod:
			self.locTitleVar.set("Incomplete Achievements")
			self.unlocTitleVar.set("Complete Achievements")
			self.setImagesGrid()
		else:
			self.locTitleVar.set("Incomplete")
			self.unlocTitleVar.set("Complete")
			self.setImagesBlend()
		self.locTitle.config(wraplength=self.winfo_width()/2)
		self.unlocTitle.config(wraplength=self.winfo_width()/2)
		self.GeneralInfo.config(wraplength=self.winfo_width())


	def changeDraw(self):
		if self.updateMethod:
			self.updateMethod=0
		else:
			self.updateMethod=1
		self.update()

	"""
	Not done.
	"""
	def setImagesBlend(self):
		unlocKeys = sorted(self.unlocked.keys())
		locKeys = sorted(self.locked.keys())
		for i,key in enumerate(unlocKeys):
			tag=self.tag(key)
			self.getImage(key,0)
		for i,key in enumerate(locKeys):
			tag = self.tag(key)
			self.getImage(key,1)
		locImg=[0,1]
		unlocImg=[0,1]
		initial=True
		tag="IMG"
		count=0
		lockedCanvas=unlockedCanvas=None
		if len(locKeys)!=0:
			lockedCanvas = tk.Canvas(self.lockedFrame, bd=0, highlightthickness=0)
			lockedCanvas.configure(background=self.color[1])
			lockedCanvas.grid()#row=0,column=0,padx=2,pady=2)
		if len(unlocKeys)!=0:
			unlockedCanvas = tk.Canvas(self.unlockedFrame, bd=0, highlightthickness=0)
			unlockedCanvas.configure(background=self.color[1])
			unlockedCanvas.grid()#row=0,column=0,padx=2,pady=2)
		newLocImg = newUnlocImg = tkLocImg = tkUnlocImg = None
		self.blendData = [(lockedCanvas,locImg,locKeys,newLocImg,tkLocImg),(unlockedCanvas,unlocImg,unlocKeys,newUnlocImg,tkUnlocImg),initial,tag,count]
		self.__newImg()

	def __newImg(self):
		lockedCanvas,locImg,locKeys,newLocImg,tkLocImg = self.blendData[0]
		unlockedCanvas,unlocImg,unlocKeys,newUnlocImg,tkUnlocImg = self.blendData[1]
		initial = self.blendData[2]
		tag = self.blendData[3]
		count = 0
		newLocImg = newUnlocImg = None
		if len(locKeys)>0:
			achieveText="{}: {}".format(locKeys[locImg[1]],self.locked[locKeys[locImg[1]]][0])
			if self.locked[locKeys[locImg[1]]][0].strip()=="":
				achieveText="{}: {}".format(locKeys[locImg[1]],"(No Description - maybe a hidden achievement)")
			locImg[0]%=len(locKeys)
			locImg[1]%=len(locKeys)
			newLocImg = copy.deepcopy(self.locked[locKeys[locImg[0]]][2])
			#tkLocImg = ImageTk.PhotoImage(newLocImg)
			#if not initial:
			#	lockedCanvas.delete(tag)
			lockedCanvas.bind("<Enter>",lambda e, text=achieveText: self.setInfo(text))
			lockedCanvas.bind("<Leave>",lambda e: self.setInfo(""))
			#lockedCanvas.create_image((0, 0), image=tkLocImg, anchor=tk.NW, tags=tag)
			#lockedCanvas.config(width=tkLocImg.width(), height=tkLocImg.height())
			#self.lockedFrame.config(width=tkLocImg.width(), height=tkLocImg.height())
		if len(unlocKeys)>0:
			achieveText="{}: {}".format(unlocKeys[unlocImg[1]],self.unlocked[unlocKeys[unlocImg[1]]][0])
			if self.unlocked[unlocKeys[unlocImg[1]]][0].strip()=="":
				achieveText="{}: {}".format(unlocKeys[unlocImg[1]],"(No Description - maybe a hidden achievement)")
			unlocImg[0]%=len(unlocKeys)
			unlocImg[1]%=len(unlocKeys)
			newUnlocImg = copy.deepcopy(self.unlocked[unlocKeys[unlocImg[0]]][2])
			#tkUnlocImg = ImageTk.PhotoImage(newUnlocImg)
			#if not initial:
			#	unlockedCanvas.delete(tag)
			unlockedCanvas.bind("<Enter>",lambda e, text=achieveText: self.setInfo(text))
			unlockedCanvas.bind("<Leave>",lambda e: self.setInfo(""))
			#unlockedCanvas.create_image((0, 0), image=tkUnlocImg, anchor=tk.NW, tags=tag)
			#unlockedCanvas.config(width=tkUnlocImg.width(), height=tkUnlocImg.height())
			#self.unlockedFrame.config(width=tkUnlocImg.width(), height=tkUnlocImg.height())
		self.blendData = [(lockedCanvas,locImg,locKeys,newLocImg,tkLocImg),(unlockedCanvas,unlocImg,unlocKeys,newUnlocImg,tkUnlocImg),initial,tag,count]
		self.blendResize()
		if initial:
			initial=False
		self.blendData = [(lockedCanvas,locImg,locKeys,newLocImg,tkLocImg),(unlockedCanvas,unlocImg,unlocKeys,newUnlocImg,tkUnlocImg),initial,tag,count]
		self.__blend()

	def __blend(self):
		lockedCanvas,locImg,locKeys,newLocImg,tkLocImg = self.blendData[0]
		unlockedCanvas,unlocImg,unlocKeys,newUnlocImg,tkUnlocImg = self.blendData[1]
		initial = self.blendData[2]
		tag = self.blendData[3]
		count = self.blendData[4]
		maxcount = (self.transition+1)*5
		alpha = count/maxcount
		if newLocImg!=None:
			newLocImg = Image.blend(self.locked[locKeys[locImg[0]]][2],self.locked[locKeys[locImg[1]]][2],alpha)
			#tkLocImg = ImageTk.PhotoImage(newLocImg)
			#lockedCanvas.delete(tag)
			#lockedCanvas.create_image((0, 0), image=tkLocImg, anchor=tk.NW, tags=tag)
			#lockedCanvas.config(width=tkLocImg.width(), height=tkLocImg.height())
		if newUnlocImg!=None:
			newUnlocImg = Image.blend(self.unlocked[unlocKeys[unlocImg[0]]][2],self.unlocked[unlocKeys[unlocImg[1]]][2],alpha)
			#tkUnlocImg = ImageTk.PhotoImage(newUnlocImg)
			#unlockedCanvas.delete(tag)
			#unlockedCanvas.create_image((0, 0), image=tkUnlocImg, anchor=tk.NW, tags=tag)
			#unlockedCanvas.config(width=tkUnlocImg.width(), height=tkUnlocImg.height())
		count+=1
		self.blendData = [(lockedCanvas,locImg,locKeys,newLocImg,tkLocImg),(unlockedCanvas,unlocImg,unlocKeys,newUnlocImg,tkUnlocImg),initial,tag,count]
		self.blendResize()
		if newUnlocImg!=None or newLocImg!=None:
			if count>maxcount:
				locImg[0]+=1
				locImg[1]+=1
				unlocImg[0]+=1
				unlocImg[1]+=1
				self._timer = self.master.after(int(self.interval)*1000,self.__newImg)
			else:
				self._timer = self.master.after(int(self.transition)*int(1000/maxcount),self.__blend)

	def setImagesGrid(self):
		unlocKeys = sorted(self.unlocked.keys())
		locKeys = sorted(self.locked.keys())
		unlocSize = int(math.sqrt(len(unlocKeys)))
		locSize = int(math.sqrt(len(locKeys)))
		if unlocSize==0:
			unlocSize=1
		if locSize==0:
			locSize=1
		for i in range(unlocSize):
			self.unlockedFrame.columnconfigure(i,pad=0,weight=1)
		for i in range(int(len(self.unlocked)/unlocSize+1)):
			self.unlockedFrame.rowconfigure(i,pad=0,weight=1)
		for i in range(locSize):
			self.lockedFrame.columnconfigure(i,pad=0,weight=1)
		for i in range(int(len(self.locked)/locSize+1)):
			self.lockedFrame.rowconfigure(i,pad=0,weight=1)
		#print(unlocSize,locSize)
		#print(len(unlocKeys),len(locKeys))
		for i,key in enumerate(unlocKeys):
			achieveText="{}: {}".format(key,self.unlocked[key][0])
			if self.unlocked[key][0].strip()=="":
				achieveText="{}: {}".format(key,"(No Description - maybe a hidden achievement)")
			tag = self.tag(key)
			self.getImage(key,0)
			self.unlocked[key].append(tk.Canvas(self.unlockedFrame, bd=0, highlightthickness=0))
			self.unlocked[key][3].configure(background=self.color[1])
			self.unlocked[key].append(ImageTk.PhotoImage(self.unlocked[key][2]))
			self.unlocked[key][3].bind("<Enter>",lambda e, text=achieveText: self.setInfo(text))
			self.unlocked[key][3].bind("<Leave>",lambda e: self.setInfo(""))
			#self.unlocked[key].append(HoverInfo(self.unlocked[key][3],key))
			self.unlocked[key][3].create_image((0, 0), image=self.unlocked[key][4], anchor=tk.NW, tags=tag)
			#print(i/unlocSize,i%unlocSize)
			#print(self.unlocked[key][4].width())
			self.unlocked[key][3].config(width=self.unlocked[key][4].width(), height=self.unlocked[key][4].height())
			self.unlocked[key][3].grid(row=i/unlocSize,column=i%unlocSize, sticky=tk.W+tk.E+tk.N+tk.S, padx=2, pady=2)
		for i,key in enumerate(locKeys):
			achieveText="{}: {}".format(key,self.locked[key][0])
			if self.locked[key][0].strip()=="":                                                                                        
				achieveText="{}: {}".format(key,"(No Description - maybe a hidden achievement)")
			tag = self.tag(key)
			self.getImage(key,1)
			self.locked[key].append(tk.Canvas(self.lockedFrame, bd=0, highlightthickness=0))
			self.locked[key][3].configure(background=self.color[1])
			self.locked[key].append(ImageTk.PhotoImage(self.locked[key][2]))
			self.locked[key][3].bind("<Enter>",lambda e, text="{}: {}".format(key,self.locked[key][0]): self.setInfo(text))
			self.locked[key][3].bind("<Leave>",lambda e: self.setInfo(""))
			#self.locked[key].append(HoverInfo(self.locked[key][3],key))
			self.locked[key][3].create_image((0, 0), image=self.locked[key][4], anchor=tk.NW, tags=tag)
			#print(i/locSize,i%locSize)
			#print(self.locked[key][4].width())
			self.locked[key][3].config(width=self.locked[key][4].width(), height=self.locked[key][4].height())
			self.locked[key][3].grid(row=i/locSize,column=i%locSize, sticky=tk.W+tk.E+tk.N+tk.S, padx=2, pady=2)
		#print self.unlocked,self.locked
		self.pack(fill=tk.BOTH, expand=1)
	
	def getImage(self,key,lockOrUnlock):
		url = ""
		if lockOrUnlock:
			url = self.locked[key][1]
		else:
			url = self.unlocked[key][1]
		image_bytes = urlopen(url).read()
		data_stream = io.BytesIO(image_bytes)
		if lockOrUnlock:
			self.locked[key].append(Image.open(data_stream))
		else:
			self.unlocked[key].append(Image.open(data_stream))
def main():
	root = tk.Tk()
	#root.configure(background='black')
	
	def on_closing():
		if messagebox.askokcancel("Quit", "Do you want to quit?"):
			root.destroy()
	
	app = App(root)
	root.protocol("WM_DELETE_WINDOW", on_closing)
	app.mainloop()
	try:
		root.destroy()
	except tk._tkinter.TclError:
		pass

if __name__=="__main__":
	main()
