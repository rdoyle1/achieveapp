First, clone this repoitory.

```bash
$ git clone https://rdoyle1@bitbucket.org/rdoyle1/achieveapp.git
```

Now, to make this work with the steam API, do the following:

clone smiley's steamapi repository

```bash
$ git clone https://github.com/smiley/steamapi.git
```

Move the modified app file (so we can have access to the descriptions and image addresses) into the folder

```bash
$ mv achieveApp/appmod.py steamapi/steamapi/app.py
```

Go into the steamapi folder and set it up

```bash
$ cd steamapi
$ python setup.py install
```

For windows, run the command from a command prompt (Start -> Accessories)

```bash
$ setup.py install
```

Now that's done, you're pretty much set up! Depending on python installations, you might need some modules, but I'm not sure what those would be off the top of my head. I'm pretty sure most of this is on a default python build.
