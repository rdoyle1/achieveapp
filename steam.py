import steamapi
from steamapi.errors import *
class Steam():
	def __init__(self,key):
		self.connection = steamapi.core.APIConnection(api_key=key)
		self.me = None
	
	def __isInt(self,val):
		try:
			int(val)
		except ValueError:
			return False
		return True

	def user(self,usr):
		if self.__isInt(usr):
			try:
				self.me = steamapi.user.SteamUser(usr)
			except UserNotFoundError:
				return 1
			except APIPrivate:
				return 2
		else:
			try:
				self.me = steamapi.user.SteamUser(userurl = self.__str(usr))
			except UserNotFoundError:
				return 1
			except APIPrivate:
				return 2
		return 0

	def __str(self,val):
		return val.encode('utf-8')

	def gameAchievements(self,gameReq):
		if self.me==None:
			return "","",{},{}
		gameIndex = -1
		try:
			if self.__isInt(gameReq):
				gameIndex = [game._id for game in self.me.games].index(gameReq)
			else:
				gameReq = self.__str(gameReq).strip().lower()
				gameIndex = [self.__str(game.name).strip().lower() for game in self.me.games].index(gameReq)
		except ValueError:
			raise ValueError([self.__str(game.name).strip().lower() for game in self.me.games])
		if gameIndex<0:
			return "","",{},{}
		unlocked = {}
		locked = {}
		game = self.me.games[gameIndex]
		try:
			for achievement in game.achievements:
				if achievement.is_unlocked:
					try:
						unlocked[self.__str(achievement.name)]=[self.__str(achievement.description),self.__str(achievement.icon)]
					except AttributeError:
						unlocked[self.__str(achievement.name)]=["",""]
				elif not achievement.is_hidden:
					try:
						locked[self.__str(achievement.name)]=[self.__str(achievement.description),self.__str(achievement.icongray)]
					except AttributeError:
						locked[self.__str(achievement.name)]=["",""]
		except APIBadCall:
			raise AttributeError("{} has no achievements.".format(gameReq))
		return self.__str(game.name),self.__str(self.me.name),unlocked,locked
